﻿
#ifndef APPLAUNCHER_H
#define APPLAUNCHER_H
#include <QObject>

//应用程序启动器
class AppLauncher : public QObject
{
    Q_OBJECT
public:
    enum AppType {
        //气体项目
        GASApp,
        //溢水项目
        YSApp,
    };
    Q_ENUM(AppType)
    explicit AppLauncher(QObject *parent = nullptr);

public:
   Q_INVOKABLE void setAppType(AppType type);
   Q_INVOKABLE AppType getAppType();
signals:

private:
    AppType currentApp_;

};

#endif // APPLAUNCHER_H
