﻿import QtQuick 2.14
import QtQuick.Window 2.14
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.14
import QtQuick.Controls.Material 2.12
import App 1.0
import App.NetWorkManager 1.0
import "qrc:/common"
import "qrc:/common/qmlQianHints"
import "qrc:/common/qmlQianDialog"
Item {
    id:root
    property int leftWidth: 182
    property int fontsize: 19
    ScrollView {
        id:flickable
        anchors.fill: parent
        clip:         true
        ScrollBar.vertical.interactive: true
        ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
                ColumnLayout {
                    id: contents
                    anchors.margins: 30
                    anchors.fill: parent
                    spacing: 20
                    RowLayout{
                        spacing:20
                        YaheiText {
                            anchors.centerIn: parent.Center
                            text:qsTr("温度上限")
                            font.pixelSize: fontsize
                            Layout.preferredWidth: leftWidth
                            Layout.alignment: Qt.AlignTop | Qt.AlignLeft
                        }
                        BaseTextField {
                            id:temp_up
                            Layout.preferredWidth:280
                            color: acceptableInput  ? "black" : "#ff0000"
                            validator: DoubleValidator {
                                  bottom: -65535.0
                                  top: 65535.0
                            }
                            onEditingFinished: {
                                if(acceptableInput) {
                                    App.protoManager.TempUpValue = text
                                }
                            }
                            text :App.protoManager.TempUpValue
                        }

                    }
                    RowLayout{
                        spacing:20
                        Layout.fillWidth: true
                        YaheiText {
                            text:qsTr("温度下限")
                            font.pixelSize: fontsize
                            Layout.preferredWidth: leftWidth
                            Layout.alignment:  Qt.AlignTop |Qt.AlignLeft
                        }
                        BaseTextField{
                            id:temp_down
                            Layout.preferredWidth:280
                            color: acceptableInput  ? "black" : "#ff0000"
                            validator: DoubleValidator {
                                  bottom: -65535.0
                                  top: 65535.0
                            }
                            onEditingFinished: {
                                if(acceptableInput) {
                                    App.protoManager.TempDownValue = text
                                }
                            }
                            text : App.protoManager.TempDownValue
                        }
                    }
                    RowLayout{
                        spacing:20
                        YaheiText {
                            anchors.centerIn: parent.Center
                            text:qsTr("湿度上限")
                            font.pixelSize: fontsize
                            Layout.preferredWidth: leftWidth
                            Layout.alignment: Qt.AlignTop | Qt.AlignLeft
                        }
                        BaseTextField{
                            id:hum_up
                            Layout.preferredWidth:280
                            color: acceptableInput  ? "black" : "#ff0000"
                            maximumLength: 32
                            validator: DoubleValidator {
                                  bottom: -65535.0
                                  top: 65535.0
                            }
                            onEditingFinished: {
                                if(acceptableInput) {
                                    App.protoManager.HumUpValue = text
                                }
                            }
                            text : App.protoManager.HumUpValue
                        }
                    }
                    RowLayout{
                        spacing:20
                        YaheiText {
                            anchors.centerIn: parent.Center
                            text:qsTr("湿度下限")
                            font.pixelSize: fontsize
                            Layout.preferredWidth: leftWidth
                            Layout.alignment: Qt.AlignTop | Qt.AlignLeft
                        }
                        BaseTextField{
                            id:hum_down
                            Layout.preferredWidth:280
                            color: acceptableInput  ? "black" : "#ff0000"
                            maximumLength: 32
                            validator: DoubleValidator {
                                  bottom: -65535.0
                                  top: 65535.0
                            }
                            onEditingFinished: {
                                if(acceptableInput) {
                                    App.protoManager.HumDownValue = text
                                }
                            }
                            text : App.protoManager.HumDownValue
                        }
                    }
                    RowLayout{
                        spacing:20
                        YaheiText {
                            anchors.centerIn: parent.Center
                            text:qsTr("水位上限")
                            font.pixelSize: fontsize
                            Layout.preferredWidth: leftWidth
                            Layout.alignment: Qt.AlignTop | Qt.AlignLeft
                        }
                        BaseTextField{
                            id: watetr_level_up
                            Layout.preferredWidth:280
                            color: acceptableInput  ? "black" : "#ff0000"
                            maximumLength: 32
                            validator: DoubleValidator {
                                  bottom: -65535.0
                                  top: 65535.0
                            }
                            onEditingFinished: {
                                if(acceptableInput) {
                                    App.protoManager.WaterLevelUpValue = text
                                }
                            }
                            text : App.protoManager.WaterLevelUpValue
                        }
                    }
                    RowLayout{
                        Layout.fillWidth: true
                        BaseButton {
                            text:qsTr("查询")
                            font.pixelSize:  20
                            backRadius: 4
                            bckcolor: "#4785FF"
                            onClicked: {
                              if(! App.protoManager.getyYiShuiSetting())
                              {
                                 message("error","查询失败")
                              }

                            }
                        }
                        Rectangle {
                             width: 200
                        }
                        BaseButton {
                            text: qsTr("设置")
                            font.pixelSize:  20
                            backRadius: 4
                            bckcolor: "#4785FF"
                            onClicked: {
                              App.protoManager.setYiShuiSetting(temp_up.text,
                                                                temp_down.text,
                                                                hum_up.text,
                                                                hum_down.text,
                                                                watetr_level_up.text)
                                {
                                   message("error","查询失败")
                                }
                            }
                        }
                    }
                    //填充最底部
                    Rectangle {
                         width: parent.width
                         height: 10
                     }
            }
    }


    Connections {
        target: App.protoManager
        // 连接信号和槽函数
        onRecvYiShuiReadSig: {
            message("success","查询成功")
        }
    }


    Connections {
        target: App.protoManager
        // 连接信号和槽函数
        onRecvYiShuiWriteSig: {
            message("success","设置成功")
        }
    }

    Message{
        id:messageTip
        z: 1
        parent: Overlay.overlay
    }
    function message(type, message) {
        if(type!=='success'&&type!=='error'&&type!=='info'){
            return false
        }
        messageTip.open(type, message)
    }

    SkinQianDialog {
        id: skinQianDialog
        backParent: windowEntry
        parent: Overlay.overlay
        onAccept: {
           skinQianDialog.close();
        }
    }
        Item {
            Layout.fillHeight: true
            Layout.fillWidth: true
        }

}
