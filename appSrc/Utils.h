﻿
#ifndef UTILS_H
#define UTILS_H

#include <QObject>
#include <QDebug>
#include <mutex>

#define Q_PROPERTY_AUTO(TYPE, M)         \
Q_PROPERTY(TYPE M MEMBER M##_ NOTIFY M##Changed)    \
    public:                         \
    Q_SIGNAL void M##Changed();      \
    void M(TYPE in##M##_)        \
{                    \
        M##_ = in##M##_;   \
        Q_EMIT M##Changed();  \
}    \
    TYPE M()    \
{          \
        return M##_; \
}     \
    private:   \
    TYPE M##_;

#define Q_PROPERTY_READONLY(TYPE, M)         \
Q_PROPERTY(TYPE M READ M CONSTANT)    \
    public:                         \
    TYPE M()                   \
{          \
        return M##_; \
}     \
    private:   \
    TYPE M##_;



template <typename T>
class Singleton {
public:
    static T* Instance();

    Singleton(const Singleton& other) = delete;
    Singleton<T>& operator=(const Singleton& other) = delete;

private:
    static std::mutex mutex;
    static T* instance;
};

template <typename T>
std::mutex Singleton<T>::mutex;
template <typename T>
T* Singleton<T>::instance;
template <typename T>
T* Singleton<T>::Instance() {
    if (instance == nullptr) {
        std::lock_guard<std::mutex> locker(mutex);
        if (instance == nullptr) {
            instance = new T();
        }
    }
    return instance;
}

#define SINGLETONG(Class)                              \
private:                                               \
    friend class Singleton<Class>;              \
    friend struct QScopedPointerDeleter<Class>;        \
                                                       \
    public:                                                \
    static Class* Instance() {                      \
        return Singleton<Class>::Instance(); \
}


class Utils : public QObject
{
    Q_OBJECT
public:
    explicit Utils(QObject *parent = nullptr);

    static void splitQuint16(quint16 value, char& highByte, char& lowByte) {
        highByte = static_cast<char>((value >> 8) & 0xFF);
        lowByte = static_cast<char>(value & 0xFF);
    }


signals:

};

#endif // UTILS_H
