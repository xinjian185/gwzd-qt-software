﻿
#include "AppLauncher.h"

AppLauncher::AppLauncher(QObject *parent)
    : QObject{parent}
{

}

void AppLauncher::setAppType(AppType type)
{
    if(type != currentApp_){
        currentApp_  = type;
    }
}

AppLauncher::AppType AppLauncher::getAppType()
{
   return currentApp_;
}

